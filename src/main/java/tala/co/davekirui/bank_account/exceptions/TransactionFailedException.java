package tala.co.davekirui.bank_account.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Failed to complete transaction")  // 500
public class TransactionFailedException extends RuntimeException {
}