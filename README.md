### Bank Account ###
Language : Java

Framework: Spring (Spring Boot)

Dependency Management : maven

Build : maven

DB : In-memory database : h2

Documentation : swagger

code coverage : jacoco

Logging : log4j

### Set Up ###
*****************************************************************************************
Build (Maven)

1.Navigate to bank_account directory (project directory)

2.Run the command : mvn package

3.A bank_account-0.0.1-SNAPSHOT.jar will be built and placed on bank_account/target 
  directory

*****************************************************************************************

Run

1.Navigate to bank_account/target directory 

2.Run the command : java -jar bank_account-0.0.1-SNAPSHOT.jar

*****************************************************************************************

Tests

1.Navigate to bank_account/

2.Run the command : mvn clean test

3.View test report output on the terminal

4.Code coverage jacoco.exec file created under bank_account/target directory

*****************************************************************************************

Coverage Test 

5.Run the command to view reports: mvn sonar:sonar

*****************************************************************************************

API Documentation (Based on Swagger)

1.With the jar application running

2.Access on browser : http://localhost:8080/bank_account/swagger-ui.html

*****************************************************************************************


### REST API ###

*****************************************************************************************
Balance;

Method : GET
Request Url : http://localhost:8080/bank_account/account/balance
Response :  {
  "id": 1,
  "accNumber": "ACC001",
  "balance": 0
}

HTTP Response code : 
200 OK

HTTP Error codes : 
401	Unauthorized
403	Forbidden
404	Not Found

*****************************************************************************************

Deposit;

Method : PUT
Request Url : http://localhost:8080/bank_account/account/deposit

Request Body : {"amount":"500"} 

Response Body (success): {
  "data": {
    "id": 1,
    "accNumber": "ACC001",
    "balance": 500
  }
}

Response Body (Error) : 
{
  "message": "Exceeded Maximum Deposit Per Day"
}



HTTP Response code : 
201  Created

HTTP Error codes : 
401	Unauthorized
403	Forbidden
404	Not Found

*****************************************************************************************

Withdrawal;

Method : PUT
Request Url : http://localhost:8080/bank_account/account/withdraw

Request Body : {"amount":"500"} 

Response Body (success): {
  "data": {
    "id": 1,
    "accNumber": "ACC001",
    "balance": 500
  }
}

Response Body (Error) : 
{
  "message": "Exceeded Maximum Withdrawal Per Day"
}



HTTP Response code : 
201  Created

HTTP Error codes : 
401	Unauthorized
403	Forbidden
404	Not Found
*****************************************************************************************

### Contribution guidelines ###


### Who do I talk to? ###

davekirui@gmail.com