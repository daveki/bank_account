package tala.co.davekirui.bank_account.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tala.co.davekirui.bank_account.entities.Account;

@RepositoryRestResource
public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findByAccNumber(String accNumber);
}