package tala.co.davekirui.bank_account;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import tala.co.davekirui.bank_account.entities.Account;
import tala.co.davekirui.bank_account.entities.AccountingEntry;
import tala.co.davekirui.bank_account.entities.Transaction;
import tala.co.davekirui.bank_account.repositories.AccountRepository;
import tala.co.davekirui.bank_account.repositories.AccountingEntryRepository;
import tala.co.davekirui.bank_account.repositories.TransactionRepository;
import tala.co.davekirui.bank_account.services.AccountService;
import tala.co.davekirui.bank_account.services.TransactionService;
import tala.co.davekirui.bank_account.util.DateTime;
import tala.co.davekirui.bank_account.util.GlobalVars;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by david on 7/17/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountServiceTest {

    @Autowired
    AccountService accountService;

    @MockBean
    AccountRepository accountRepository;

    @MockBean
    AccountingEntryRepository accountingEntryRepository;

    @MockBean
    TransactionService transactionService;

    @Test
    public void testMulformedWithdrawFundsObject() {
        Transaction transaction = new Transaction();
        Account account = accountService.withdrawFunds(transaction);
        Assert.assertEquals(account, null);
    }

    @Test
    public void testSuccessfulWithdrawFundsObject() {
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(10000));
        transaction.setAccNumber(GlobalVars.ACC_NUM);
        transaction.setTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString());
        transaction.setDate(DateTime.convertDateToTimestamp(new Date()));

        //Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(10000));

        //Accounting Entry
        AccountingEntry accountingEntry = new AccountingEntry();
        accountingEntry.setAccNumber(GlobalVars.ACC_NUM);
        accountingEntry.setRunningBalance(new BigDecimal(10000));
        accountingEntry.setCredit(new BigDecimal(10000));
        accountingEntry.setDebit(new BigDecimal(0));

        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);
        when(accountRepository.save(account)).thenReturn(account);
        when(accountingEntryRepository.save(accountingEntry)).thenReturn(accountingEntry);
        when(transactionService.saveTransaction(transaction)).thenReturn(transaction);

        Account acc = accountService.withdrawFunds(transaction);
        Assert.assertNotNull(account);
    }

}
