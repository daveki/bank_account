package tala.co.davekirui.bank_account;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import tala.co.davekirui.bank_account.controllers.AccountController;
import tala.co.davekirui.bank_account.entities.Account;
import tala.co.davekirui.bank_account.entities.AccountingEntry;
import tala.co.davekirui.bank_account.entities.Transaction;
import tala.co.davekirui.bank_account.repositories.AccountRepository;
import tala.co.davekirui.bank_account.repositories.AccountingEntryRepository;
import tala.co.davekirui.bank_account.services.AccountService;
import tala.co.davekirui.bank_account.services.TransactionService;
import tala.co.davekirui.bank_account.util.DateTime;
import tala.co.davekirui.bank_account.util.GlobalVars;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by david on 7/17/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AccountService accountService;

    @MockBean
    TransactionService transactionService;

    @MockBean
    AccountRepository accountRepository;

    @MockBean
    AccountingEntryRepository accountingEntryRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Test
    public void testBalance() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(0));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Test
        mockMvc.perform(get("/account/balance")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance", is(0)));
    }


    @Test
    public void testDepositWithDailyTransactionLimit() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(100000));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Mock Todays Transactions
        List<Transaction> transactions = new ArrayList<>();
        Transaction today_trx1 = new Transaction();
        today_trx1.setAmount(new BigDecimal(160000));
        today_trx1.setAccNumber(GlobalVars.ACC_NUM);
        today_trx1.setTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString());
        today_trx1.setDate(DateTime.convertDateToTimestamp(new Date()));
        transactions.add(today_trx1);
        when(transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString())).thenReturn(transactions);

        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(100));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void testDepositWithMaxDailyFreq() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(4000));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Mock Todays Transactions
        List<Transaction> transactions = new ArrayList<>();
        Transaction today_trx1 = new Transaction();
        today_trx1.setAmount(new BigDecimal(1000));
        today_trx1.setAccNumber(GlobalVars.ACC_NUM);
        today_trx1.setTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString());
        today_trx1.setDate(DateTime.convertDateToTimestamp(new Date()));
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        when(transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString())).thenReturn(transactions);

        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(1000));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void testDepositWithSingleTransactionLimit() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(10000));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Mock Todays Transactions
        List<Transaction> transactions = new ArrayList<>();
        Transaction today_trx1 = new Transaction();
        today_trx1.setAmount(new BigDecimal(80000));
        today_trx1.setAccNumber(GlobalVars.ACC_NUM);
        today_trx1.setTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString());
        today_trx1.setDate(DateTime.convertDateToTimestamp(new Date()));
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        when(transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString())).thenReturn(transactions);


        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(22000));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }


    @Test
    public void testDepositSuccessfully() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(10000));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Accounting Entry
        AccountingEntry accountingEntry = new AccountingEntry();
        accountingEntry.setAccNumber(GlobalVars.ACC_NUM);
        accountingEntry.setRunningBalance(new BigDecimal(10000));
        accountingEntry.setCredit(new BigDecimal(10000));
        accountingEntry.setDebit(new BigDecimal(0));


        //Mock Todays Transactions
        List<Transaction> transactions = new ArrayList<>();
        Transaction today_trx1 = new Transaction();
        today_trx1.setAmount(new BigDecimal(10000));
        today_trx1.setAccNumber(GlobalVars.ACC_NUM);
        today_trx1.setTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString());
        today_trx1.setDate(DateTime.convertDateToTimestamp(new Date()));
        transactions.add(today_trx1);
        when(transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString())).thenReturn(transactions);

        //Mock Persist
        Account fakeValue = new Account();
        fakeValue.setAccNumber(GlobalVars.ACC_NUM);
        fakeValue.setBalance(new BigDecimal(10000));

        when(accountService.depositFunds(today_trx1)).thenReturn(fakeValue);

        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(2000));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
    }


    @Test
    public void testWithdrawalWithInsufficientFunds() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(100));
        account.setId(new Long(3));

        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);
        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(5000));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void testWithdrawalWithDailyTransactionLimit() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(100000));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Mock Todays Transactions
        List<Transaction> transactions = new ArrayList<>();
        Transaction today_trx1 = new Transaction();
        today_trx1.setAmount(new BigDecimal(60000));
        today_trx1.setAccNumber(GlobalVars.ACC_NUM);
        today_trx1.setTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString());
        today_trx1.setDate(DateTime.convertDateToTimestamp(new Date()));
        transactions.add(today_trx1);
        when(transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString())).thenReturn(transactions);

        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(100));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void testWithdrawalWithMaxDailyFreq() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(4000));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Mock Todays Transactions
        List<Transaction> transactions = new ArrayList<>();
        Transaction today_trx1 = new Transaction();
        today_trx1.setAmount(new BigDecimal(1000));
        today_trx1.setAccNumber(GlobalVars.ACC_NUM);
        today_trx1.setTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString());
        today_trx1.setDate(DateTime.convertDateToTimestamp(new Date()));
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        when(transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString())).thenReturn(transactions);

        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(1000));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void testWithdrawalWithSingleTransactionLimit() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(10000));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Mock Todays Transactions
        List<Transaction> transactions = new ArrayList<>();
        Transaction today_trx1 = new Transaction();
        today_trx1.setAmount(new BigDecimal(1000));
        today_trx1.setAccNumber(GlobalVars.ACC_NUM);
        today_trx1.setTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString());
        today_trx1.setDate(DateTime.convertDateToTimestamp(new Date()));
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        transactions.add(today_trx1);
        when(transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString())).thenReturn(transactions);


        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(22000));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isUnprocessableEntity());
    }


    @Test
    public void testWithdrawalSuccessfully() throws Exception {

        //Mock Bank Account
        Account account = new Account();
        account.setAccNumber(GlobalVars.ACC_NUM);
        account.setBalance(new BigDecimal(10000));
        when(accountService.findAccountByAccNumber(GlobalVars.ACC_NUM)).thenReturn(account);

        //Accounting Entry
        AccountingEntry accountingEntry = new AccountingEntry();
        accountingEntry.setAccNumber(GlobalVars.ACC_NUM);
        accountingEntry.setRunningBalance(new BigDecimal(10000));
        accountingEntry.setCredit(new BigDecimal(10000));
        accountingEntry.setDebit(new BigDecimal(0));


        //Mock Todays Transactions
        List<Transaction> transactions = new ArrayList<>();
        Transaction today_trx1 = new Transaction();
        today_trx1.setAmount(new BigDecimal(10000));
        today_trx1.setAccNumber(GlobalVars.ACC_NUM);
        today_trx1.setTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString());
        today_trx1.setDate(DateTime.convertDateToTimestamp(new Date()));
        transactions.add(today_trx1);
        when(transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString())).thenReturn(transactions);

        //Mock Persist
        Account fakeValue = new Account();
        fakeValue.setAccNumber(GlobalVars.ACC_NUM);
        fakeValue.setBalance(new BigDecimal(10000));
        fakeValue.setId(3);
        when(accountService.withdrawFunds(today_trx1)).thenReturn(fakeValue);

        //Test
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(2000));

        Gson gson = new Gson();
        String json = gson.toJson(transaction);

        mockMvc.perform(put("/account/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
    }
}
