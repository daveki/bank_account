package tala.co.davekirui.bank_account.controllers;

import io.swagger.annotations.Api;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tala.co.davekirui.bank_account.entities.Account;
import tala.co.davekirui.bank_account.entities.Transaction;
import tala.co.davekirui.bank_account.exceptions.TransactionFailedException;
import tala.co.davekirui.bank_account.models.Response;
import tala.co.davekirui.bank_account.services.AccountService;
import tala.co.davekirui.bank_account.services.TransactionService;
import tala.co.davekirui.bank_account.util.DateTime;
import tala.co.davekirui.bank_account.util.ErrorMessages;
import tala.co.davekirui.bank_account.util.GlobalVars;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by david on 7/19/17.
 */

@RestController
@RequestMapping("/account")
@Api(value = "bank account", description = "Operations pertaining to a bank account")
public class AccountController {//API end points

    private static final Logger logger = Logger.getLogger(AccountController.class);

    @Autowired
    AccountService accountService;

    @Autowired
    TransactionService transactionService;

    @RequestMapping(value = "/balance", method = RequestMethod.GET)
    public ResponseEntity<Account> findAccountBalance() {//account balance end point
        Account account = accountService.findAccountByAccNumber(GlobalVars.ACC_NUM);
        if (account == null)
            return new ResponseEntity(HttpStatus.NO_CONTENT);

        return new ResponseEntity<>(account, HttpStatus.OK);
    }

    @RequestMapping(value = "/deposit", method = RequestMethod.PUT)
    public ResponseEntity<Response> depositFunds(@RequestBody Transaction transaction) {//deposit end point
        Response response = new Response();

        //build deposit transaction
        transaction.setDate(DateTime.convertDateToTimestamp(new Date()));
        transaction.setTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString());
        transaction.setAccNumber(GlobalVars.ACC_NUM);

        //validations
        List<Transaction> transactions = transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString());//list of deposit transactions for the day
        BigDecimal bal = transactions.stream().map(Transaction::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        if ((bal.add(transaction.getAmount())).compareTo(GlobalVars.MAX_DEPOSITS_PER_DAY) == 1) {//Amount greater than MAX_DEPOSITS_PER_DAY
            response.setMessage(ErrorMessages.deposit_1);
            return new ResponseEntity(response, HttpStatus.UNPROCESSABLE_ENTITY);
        } else if (transaction.getAmount().compareTo(GlobalVars.MAX_DEPOSITS_PER_TRX) == 1) {//Amount greater than MAX_DEPOSITS_PER_TRX
            response.setMessage(ErrorMessages.deposit_2);
            return new ResponseEntity(response, HttpStatus.UNPROCESSABLE_ENTITY);
        } else if (transactions.size() >= GlobalVars.MAX_DEPOSITS_FREQ_PER_DAY) {//Amount greater than MAX_DEPOSITS_FREQ_PER_DAY
            response.setMessage(ErrorMessages.deposit_3);
            return new ResponseEntity(response, HttpStatus.UNPROCESSABLE_ENTITY);
        } else {//Transaction OK
            Account account = accountService.depositFunds(transaction);
            //if (account == null) throw new TransactionFailedException(); //failed
            response.setData(account);
            logger.info("Deposited successfully");
            return new ResponseEntity<>(response, HttpStatus.CREATED); //success
        }
    }


    @RequestMapping(value = "/withdraw", method = RequestMethod.PUT)
    public ResponseEntity<Response> withdrawFunds(@RequestBody Transaction transaction) {//deposit end point
        Response response = new Response();

        //build withdrawal transaction
        transaction.setDate(DateTime.convertDateToTimestamp(new Date()));
        transaction.setTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString());
        transaction.setAccNumber(GlobalVars.ACC_NUM);

        //validations
        List<Transaction> transactions = transactionService.findTodaysTransactionsByTrxType(GlobalVars.TransactionTypes.WITHDRAWAL.toString());//list of withdrawal transactions for the day
        BigDecimal bal = transactions.stream().map(Transaction::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
        Account account = accountService.findAccountByAccNumber(GlobalVars.ACC_NUM);
        if (transaction.getAmount().compareTo(account.getBalance()) == 1) {//Insufficient funds to process the transaction
            response.setMessage(ErrorMessages.withdrawal_0);
            return new ResponseEntity(response, HttpStatus.UNPROCESSABLE_ENTITY);
        } else if ((bal.add(transaction.getAmount())).compareTo(GlobalVars.MAX_WITHDRAW_PER_DAY) == 1) {//Amount greater than MAX_WITHDRAW_PER_DAY
            response.setMessage(ErrorMessages.withdrawal_1);
            return new ResponseEntity(response, HttpStatus.UNPROCESSABLE_ENTITY);
        } else if (transaction.getAmount().compareTo(GlobalVars.MAX_WITHDRAW_PER_TRX) == 1) {//Amount greater than MAX_WITHDRAW_PER_TRX
            response.setMessage(ErrorMessages.withdrawal_2);
            return new ResponseEntity(response, HttpStatus.UNPROCESSABLE_ENTITY);
        } else if (transactions.size() >= GlobalVars.MAX_WITHDRAW_FREQ_PER_DAY) {//Amount greater than MAX_WITHDRAW_FREQ_PER_DAY
            response.setMessage(ErrorMessages.withdrawal_3);
            return new ResponseEntity(response, HttpStatus.UNPROCESSABLE_ENTITY);
        } else {//Transaction OK
            account = accountService.withdrawFunds(transaction);
            response.setData(account);
            logger.info("Withdrawal was successful");
            return new ResponseEntity<>(response, HttpStatus.CREATED); //success
        }
    }
}
