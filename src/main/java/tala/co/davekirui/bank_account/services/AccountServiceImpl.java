package tala.co.davekirui.bank_account.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tala.co.davekirui.bank_account.entities.Account;
import tala.co.davekirui.bank_account.entities.AccountingEntry;
import tala.co.davekirui.bank_account.entities.Transaction;
import tala.co.davekirui.bank_account.repositories.AccountRepository;
import tala.co.davekirui.bank_account.repositories.AccountingEntryRepository;
import tala.co.davekirui.bank_account.util.GlobalVars;

import java.math.BigDecimal;

/**
 * Created by david on 7/19/17.
 */
@Service("account_service")
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountingEntryRepository accountingEntryRepository;

    @Autowired
    TransactionService transactionService;

    private static final Logger logger = Logger.getLogger(AccountServiceImpl.class);

    @Override
    public Account findAccountByAccNumber(String accNumber) {
        return accountRepository.findByAccNumber(accNumber);
    }


    @Override
    public Account depositFunds(Transaction transaction) {
        try {
            //Update account balance
            Account account = findAccountByAccNumber(GlobalVars.ACC_NUM);
            account.setBalance(account.getBalance().add(transaction.getAmount())); //Add account balance - deposit

            //perform accounting entries
            AccountingEntry accountingEntry = new AccountingEntry();
            accountingEntry.setAccNumber(transaction.getAccNumber());
            accountingEntry.setCredit(transaction.getAmount());
            accountingEntry.setDebit(BigDecimal.ZERO);
            accountingEntry.setRunningBalance(account.getBalance());

            //Atomic Transaction
            transactionService.saveTransaction(transaction);//persist transaction to database
            accountRepository.save(account);//account update to database
            accountingEntryRepository.save(accountingEntry);//accounting entry update to database
            return account;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public Account withdrawFunds(Transaction transaction) {
        try {
            //Update account balance
            Account account = findAccountByAccNumber(GlobalVars.ACC_NUM);
            account.setBalance(account.getBalance().subtract(transaction.getAmount())); //subtract account balance - withdrawal

            //perform accounting entries
            AccountingEntry accountingEntry = new AccountingEntry();
            accountingEntry.setAccNumber(transaction.getAccNumber());
            accountingEntry.setDebit(transaction.getAmount());
            accountingEntry.setCredit(BigDecimal.ZERO);
            accountingEntry.setRunningBalance(account.getBalance());

            //Atomic Transaction
            transactionService.saveTransaction(transaction);//persist transaction to database
            accountRepository.save(account);//account update to database
            accountingEntryRepository.save(accountingEntry);//accounting entry update to database
            return account;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
        return null;
    }

}
