package tala.co.davekirui.bank_account.util;

import java.math.BigDecimal;

/**
 * Created by david on 7/20/17.
 */
public class GlobalVars {
    public enum TransactionTypes {DEPOSIT, WITHDRAWAL}

    public static String ACC_NUM = "ACC001";

    public static BigDecimal MAX_DEPOSITS_PER_DAY = new BigDecimal(150000);
    public static BigDecimal MAX_DEPOSITS_PER_TRX = new BigDecimal(40000);
    public static int MAX_DEPOSITS_FREQ_PER_DAY = 4;

    public static BigDecimal MAX_WITHDRAW_PER_DAY = new BigDecimal(50000);
    public static BigDecimal MAX_WITHDRAW_PER_TRX = new BigDecimal(20000);
    public static int MAX_WITHDRAW_FREQ_PER_DAY = 3;



}
