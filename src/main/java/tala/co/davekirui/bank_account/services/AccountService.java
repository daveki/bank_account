package tala.co.davekirui.bank_account.services;

import tala.co.davekirui.bank_account.entities.Account;
import tala.co.davekirui.bank_account.entities.Transaction;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by david on 7/19/17.
 */
public interface AccountService { //interface for the account service
    Account findAccountByAccNumber(String accNumber);

    Account depositFunds(Transaction transaction);

    Account withdrawFunds(Transaction transaction);
}
