package tala.co.davekirui.bank_account.models;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by david on 7/20/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    Object data;
    String message;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
