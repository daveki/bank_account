package tala.co.davekirui.bank_account.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tala.co.davekirui.bank_account.entities.Transaction;
import tala.co.davekirui.bank_account.repositories.TransactionRepository;

import java.sql.Date;
import java.util.List;

/**
 * Created by david on 7/19/17.
 */
@Service("transaction_service")
@Transactional
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public List<Transaction> findTodaysTransactionsByTrxType(String trxType) {
        return transactionRepository.findTodaysTransactionsByTrxType(trxType);
    }

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        return transactionRepository.save(transaction); //Persist transaction to the database
    }
}
