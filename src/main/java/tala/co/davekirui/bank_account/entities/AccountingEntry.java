package tala.co.davekirui.bank_account.entities;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by david on 7/19/17.
 */
@Entity
@Table(name = "tbl_accounting_entries")
public class AccountingEntry { //accounting entry object
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String accNumber;

    private BigDecimal debit;

    private BigDecimal credit;

    private BigDecimal runningBalance;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public BigDecimal getDebit() {
        return debit;
    }

    public void setDebit(BigDecimal debit) {
        this.debit = debit;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getRunningBalance() {
        return runningBalance;
    }

    public void setRunningBalance(BigDecimal runningBalance) {
        this.runningBalance = runningBalance;
    }
}
