package tala.co.davekirui.bank_account.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by david on 7/20/17.
 */
public class DateTime {

    public static Timestamp convertDateToTimestamp(Date date) {
        return (new Timestamp(date.getTime()));
    }

    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return sdf.format(new Date());
    }


    public static String getFormatedTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
        return sdf.format(date);
    }

    public static String getOracleTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy hh.mm.ss.S a");
        return sdf.format(date);
    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(new Date());
    }

}
