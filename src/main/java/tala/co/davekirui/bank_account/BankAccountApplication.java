package tala.co.davekirui.bank_account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankAccountApplication {
    public static void main(String[] args) {//boot the application
        SpringApplication.run(BankAccountApplication.class, args);
    }
}


