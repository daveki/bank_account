package tala.co.davekirui.bank_account;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tala.co.davekirui.bank_account.entities.Account;
import tala.co.davekirui.bank_account.entities.Transaction;
import tala.co.davekirui.bank_account.repositories.AccountRepository;
import tala.co.davekirui.bank_account.repositories.TransactionRepository;
import tala.co.davekirui.bank_account.util.DateTime;
import tala.co.davekirui.bank_account.util.GlobalVars;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionRepositoryTest {

    @Autowired
    private TransactionRepository transactionRepository;

    @Transactional
    @Test
    public void testTransactionPersistence() {
        Transaction transaction = new Transaction();
        transaction.setAmount(new BigDecimal(160000));
        transaction.setAccNumber(GlobalVars.ACC_NUM);
        transaction.setTrxType(GlobalVars.TransactionTypes.DEPOSIT.toString());
        transaction.setDate(DateTime.convertDateToTimestamp(new Date()));


        Transaction transaction1 = transactionRepository.save(transaction);

        Assert.assertNotNull(transaction1);

        Assert.assertEquals(GlobalVars.ACC_NUM, transaction1.getAccNumber());
        Assert.assertEquals(GlobalVars.TransactionTypes.DEPOSIT.toString(), transaction1.getTrxType());
        Assert.assertEquals(new BigDecimal(160000), transaction1.getAmount());
    }
}