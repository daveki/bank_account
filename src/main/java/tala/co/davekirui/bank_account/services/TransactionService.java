package tala.co.davekirui.bank_account.services;

import tala.co.davekirui.bank_account.entities.Transaction;

import java.sql.Date;
import java.util.List;

/**
 * Created by david on 7/19/17.
 */
public interface TransactionService {

    List<Transaction> findTodaysTransactionsByTrxType(String trxType);

    Transaction saveTransaction(Transaction transaction);
}
