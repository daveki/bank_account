package tala.co.davekirui.bank_account;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tala.co.davekirui.bank_account.entities.AccountingEntry;
import tala.co.davekirui.bank_account.entities.Transaction;
import tala.co.davekirui.bank_account.repositories.AccountingEntryRepository;
import tala.co.davekirui.bank_account.repositories.TransactionRepository;
import tala.co.davekirui.bank_account.util.DateTime;
import tala.co.davekirui.bank_account.util.GlobalVars;

import java.math.BigDecimal;
import java.util.Date;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountingEntryRepositoryTest {

    @Autowired
    private AccountingEntryRepository accountingEntryRepository;

    @Transactional
    @Test
    public void testTransactionPersistence() {
        AccountingEntry accountingEntry = new AccountingEntry();
        accountingEntry.setAccNumber(GlobalVars.ACC_NUM);
        accountingEntry.setRunningBalance(new BigDecimal(10000));
        accountingEntry.setCredit(new BigDecimal(10000));
        accountingEntry.setDebit(new BigDecimal(0));
        AccountingEntry accountingEntry1 = accountingEntryRepository.save(accountingEntry);

        Assert.assertNotNull(accountingEntry1);

        Assert.assertEquals(GlobalVars.ACC_NUM, accountingEntry1.getAccNumber());
        Assert.assertEquals(new BigDecimal(10000), accountingEntry1.getCredit());
        Assert.assertEquals(new BigDecimal(0), accountingEntry1.getDebit());
        Assert.assertEquals(new BigDecimal(10000), accountingEntry1.getRunningBalance());
    }
}