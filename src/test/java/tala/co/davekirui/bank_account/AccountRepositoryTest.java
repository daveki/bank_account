package tala.co.davekirui.bank_account;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tala.co.davekirui.bank_account.entities.Account;
import tala.co.davekirui.bank_account.repositories.AccountRepository;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Transactional
    @Test
    public void testFindByAccNumber() {
        Account account = new Account();
        account.setAccNumber("AC08000");
        account.setBalance(new BigDecimal("5000"));

        accountRepository.save(account);
        Account saved_account = accountRepository.findByAccNumber("AC08000");
        Assert.assertEquals(new BigDecimal("5000"), saved_account.getBalance());
    }
}