package tala.co.davekirui.bank_account.util;

/**
 * Created by david on 7/20/17.
 */
public class ErrorMessages {
    public static String deposit_1 = "Exceeded Maximum Deposit Per Day";
    public static String deposit_2 = "Exceeded Maximum Deposit Per Transaction";
    public static String deposit_3 = "Exceeded Maximum Deposit Frequency Per Day";

    public static String withdrawal_0 = "Insufficient funds";
    public static String withdrawal_1 = "Exceeded Maximum Withdrawal Per Day";
    public static String withdrawal_2 = "Exceeded Maximum Withdrawal Per Transaction";
    public static String withdrawal_3 = "Exceeded Maximum Withdrawal Frequency Per Day";
}
