package tala.co.davekirui.bank_account.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import tala.co.davekirui.bank_account.entities.Account;
import tala.co.davekirui.bank_account.entities.Transaction;

import java.sql.Date;
import java.util.List;

@RepositoryRestResource
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Query("select t from Transaction t where t.date = current_date and t.trxType=?1 ")
    List<Transaction> findTodaysTransactionsByTrxType(String trxType);
}