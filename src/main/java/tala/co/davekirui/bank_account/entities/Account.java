package tala.co.davekirui.bank_account.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "tbl_accounts")
public class Account {//accounts object

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String accNumber;

    private BigDecimal balance;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void Account(long id, String accNumber, BigDecimal balance) {
        this.id = id;
        this.accNumber = accNumber;
        this.balance = balance;
    }

    public void Account() {
    }
}
